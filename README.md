# acamica-dwbe-sprint-project-01

### Introducción

Es la base que construímos en el webinar Sprint Project 01 de la carrera de desarrollo web back end de Acámica

### Instrucciones de instalación

```sh
git clone https://gitlab.com/danielsegovia/acamica-dwbe-sprint-project-01
cd acamica-dwbe-sprint-project-01
npm install
npm run dev
```